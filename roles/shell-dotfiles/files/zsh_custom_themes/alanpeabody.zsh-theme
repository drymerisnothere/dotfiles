local user=$'\e%{[1;35m%}%n\e%{[1;94m%} @ \e%{[1;35m%}%m%{$reset_color%}'
local pwd=$'\e%{[1;94m%}%~%{$reset_color%}'
local rvm=''
if which rvm-prompt &> /dev/null; then
  rvm='%{$fg[green]%}‹$(rvm-prompt i v g)›%{$reset_color%}'
else
  if which rbenv &> /dev/null; then
    rvm='%{$fg[green]%}‹$(rbenv version | sed -e "s/ (set.*$//")›%{$reset_color%}'
  fi
fi
local return_code='%(?..%{$fg[red]%}%? ↵%{$reset_color%})'
local git_branch='$(git_prompt_status)%{$reset_color%}$(git_prompt_info)%{$reset_color%}'

# ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[green]%}"
ZSH_THEME_GIT_PROMPT_PREFIX='%{\e[1;32m%}'
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY=""
ZSH_THEME_GIT_PROMPT_CLEAN=""

ZSH_THEME_GIT_PROMPT_ADDED="%{\e[1;32m%} ✚"
ZSH_THEME_GIT_PROMPT_MODIFIED="%{\e[1;32m%} ✹"
ZSH_THEME_GIT_PROMPT_DELETED="%{\e[1;31m%} ✖"
ZSH_THEME_GIT_PROMPT_RENAMED="%{\e[1;35m%} ➜"
ZSH_THEME_GIT_PROMPT_UNMERGED="%{\e[1;33m%} ═"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{\e[1;94m%} ✭"

PROMPT="${user} ${pwd} $ $reset_color"
RPROMPT="${return_code} ${git_branch} ${rvm}"
