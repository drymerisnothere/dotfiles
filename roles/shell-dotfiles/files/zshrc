# Term
export TERM="xterm-256color"

# Ruta de oh-my-zsh
export ZSH=$HOME/.oh-my-zsh

POWERLEVEL9K_MODE='awesome-fontconfig'
ZSH_THEME="powerlevel9k/powerlevel9k"
POWERLEVEL9K_PYTHON_ICON=""
POWERLEVEL9K_BATTERY_ICON='\uf1e6 '
POWERLEVEL9K_SHORTEN_DIR_LENGTH=1
POWERLEVEL9K_SHORTEN_STRATEGY=truncate_to_unique
POWERLEVEL9K_DIR_OMIT_FIRST_CHARACTER=false
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(battery time dir virtualenv vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(custom_exec_time status background_jobs aws kubectl_context)

# No comprobar si hay actualizaciones
DISABLE_AUTO_UPDATE="true"

# Corregir
ENABLE_CORRECTION="false"

# Mostrar puntos rojos mientras se espera el completado
COMPLETION_WAITING_DOTS="true"

# Formato de tiempo
HIST_STAMPS="dd/mm/yyyy"

# Plugins
plugins=(git colored-man-pages sprunge pip ssh-agent aws gitfast kube alias-tips copyfile jira)

# Path
PATH="/usr/local/bin:/usr/bin:/bin:/usr/games:/usr/sbin:/sbin:/usr/local/bin:$HOME/Scripts/bin/:/opt/go/bin"
export MANPATH="/usr/local/man:$MANPATH"

# Usar oh-my-zsh
source $ZSH/oh-my-zsh.sh

# Lenguaje
export LANG=es_ES.UTF-8
export LANGUAGE=es_ES.UTF-8
export LC_ALL=es_ES.UTF-8

# Editor preferido
export EDITOR="emacsclient -t -c"
export ALTERNATE_EDITOR="vim"

# Funciones personalizadas
source $HOME/.oh-my-zsh/custom/themes/custom.zsh

# Alias
source $HOME/.aliases

# Python virtualenv
export WORKON_HOME=$HOME/.virtualenvs
mkdir -p $HOME/.virtualenvs
source /usr/local/bin/virtualenvwrapper_lazy.sh > /dev/null

# GOPATH
export GOPATH=/opt/go

# Cargar funciones
source $HOME/.functions

# Filtro para less
export LESS='-R'
export LESSOPEN='|~/.lessfilter %s'

# Systemd stuff
export XDG_RUNTIME_DIR="/run/user/$UID"
export DBUS_SESSION_BUS_ADDRESS="unix:path=${XDG_RUNTIME_DIR}/bus"

# fzf stuff
source ~/.fzf.zsh
source ~/Instalados/z/z.sh
source ~/Instalados/fz/fz.plugin.zsh
source ~/Instalados/zsh-interactive-cd/zsh-interactive-cd.plugin.zsh

# automatically selects the item if there's only one
export FZF_CTRL_T_OPTS="--select-1 --exit-0"

# The following example uses tree command to show the entries of the directory
export FZF_ALT_C_OPTS="--preview 'tree -C {} | head -200'"

# Add git-extras autocompletion
source ~/Instalados/git-extras//etc/git-extras-completion.zsh
