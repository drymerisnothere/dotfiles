import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')
pip_packages = open("../../defaults/main.yml").read().replace('- ', '')\
                                                        .split()[2:]


@pytest.mark.parametrize("name", pip_packages)
def test_pip_packages(host, name):
    name = name.replace('"', '').split('[')[0]
    installed = name in host.pip_package.get_packages()
    assert installed is True
