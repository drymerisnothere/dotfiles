Pip-packages
=========

Install the pip packages I usually use.

Requirements
------------

None

Role Variables
--------------

* `pip_packages`: List of packages to install.

Dependencies
------------

None

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - { role: pip-packages }
```

Compatible
----------

With:
  - Debian 9

License
-------

GPL3

Author Information
------------------

drymer [ AT ] autistici.org
