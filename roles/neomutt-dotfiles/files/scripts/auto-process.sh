#!/bin/bash

MESSAGE=$(notmuch show tag:unread 2> /dev/null)
NEWALIAS=$(echo "$MESSAGE" | grep ^"From: ")

if [[ -n $NEWALIAS ]]
then
    while read -r line
    do
	busqueda="$(abook --datafile ~/.abook --mutt-query $(echo $line | cut -d'<' -f2 | cut -d'>' -f1 | cut -d':' -f2 | sed 's/^ //'))"

	# auto add sender to abook
	if [[ $busqueda = "Not found" ]]
	then
    	    echo $line | abook --datafile ~/.abook --add-email-quiet > /dev/null
	fi

    done < <(echo "$NEWALIAS")
fi
