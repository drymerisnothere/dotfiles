#!/bin/bash

i=0

lista=("emacs-orgmode@gnu.org" "list_criptica@inventati.org" "@lists.torproject.org")
tags=("orgmode" "criptica" "tor")

for mail in ${lista[@]}
do
    notmuch tag -inbox +${tags[$i]} --output=messages $mail tag:inbox
    i=$((i+1))
done
