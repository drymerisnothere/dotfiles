# Atajos de  teclado

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Función</th>
<th scope="col" class="org-left">Atajo</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">Abrir buscador</td>
<td class="org-left">o</td>
</tr>


<tr>
<td class="org-left">Abrir dirección actual y editar</td>
<td class="org-left">go</td>
</tr>


<tr>
<td class="org-left">Abrir nueva pestaña</td>
<td class="org-left">C-t</td>
</tr>


<tr>
<td class="org-left">Abrir nueva ventana</td>
<td class="org-left">C-n</td>
</tr>


<tr>
<td class="org-left">Cerrar pestaña</td>
<td class="org-left">C-w</td>
</tr>


<tr>
<td class="org-left">Moverse a la pestañade la izquierda</td>
<td class="org-left">C-j</td>
</tr>


<tr>
<td class="org-left">Moverse a la pestaña de la derecha</td>
<td class="org-left">C-k</td>
</tr>


<tr>
<td class="org-left">Mover la pestaña hacia la izquierda</td>
<td class="org-left">gl</td>
</tr>


<tr>
<td class="org-left">Mover la pestaña hacia la derecha</td>
<td class="org-left">gr</td>
</tr>


<tr>
<td class="org-left">Recargar página</td>
<td class="org-left">r / f5</td>
</tr>


<tr>
<td class="org-left">Ir una página hacia atras en el historial</td>
<td class="org-left">H</td>
</tr>


<tr>
<td class="org-left">Ir una página hacia delante en el historial</td>
<td class="org-left">L</td>
</tr>


<tr>
<td class="org-left">Abrir link mediante el teclado</td>
<td class="org-left">f</td>
</tr>


<tr>
<td class="org-left">Abrir link mediante el teclado en nueva pestaña</td>
<td class="org-left">F</td>
</tr>


<tr>
<td class="org-left">Abrir link mediante el teclado en nueva ventana</td>
<td class="org-left">wf</td>
</tr>


<tr>
<td class="org-left">Copiar link actual mediante el teclado</td>
<td class="org-left">;Y</td>
</tr>


<tr>
<td class="org-left">Descargar página actual</td>
<td class="org-left">C-e</td>
</tr>


<tr>
<td class="org-left">Descargar mediante teclado</td>
<td class="org-left">C-d</td>
</tr>


<tr>
<td class="org-left">Moverse hacia la izquierda</td>
<td class="org-left">h</td>
</tr>


<tr>
<td class="org-left">Moverse hacia la derecha</td>
<td class="org-left">l</td>
</tr>


<tr>
<td class="org-left">Moverse hacia arriba</td>
<td class="org-left">k</td>
</tr>


<tr>
<td class="org-left">Moverse hacia abajo</td>
<td class="org-left">j</td>
</tr>


<tr>
<td class="org-left">Reabrir pestaña cerrada</td>
<td class="org-left">C-S-t</td>
</tr>


<tr>
<td class="org-left">Buscar</td>
<td class="org-left">C-s</td>
</tr>


<tr>
<td class="org-left">Buscar siguiente</td>
<td class="org-left">n</td>
</tr>


<tr>
<td class="org-left">Buscar anterior</td>
<td class="org-left">N</td>
</tr>


<tr>
<td class="org-left">Ejecutar órdenes</td>
<td class="org-left">M-x</td>
</tr>


<tr>
<td class="org-left">Entrar en modo de insercion</td>
<td class="org-left">i</td>
</tr>


<tr>
<td class="org-left">Copiar url principal</td>
<td class="org-left">yy</td>
</tr>


<tr>
<td class="org-left">Copiar título de la página</td>
<td class="org-left">yt</td>
</tr>


<tr>
<td class="org-left">Copiar dominio</td>
<td class="org-left">yd</td>
</tr>


<tr>
<td class="org-left">Pegar url en la misma pestaña</td>
<td class="org-left">pp</td>
</tr>


<tr>
<td class="org-left">Pegar url en pestaña nueva</td>
<td class="org-left">Pp</td>
</tr>


<tr>
<td class="org-left">Pegar url en ventana nueva</td>
<td class="org-left">wp</td>
</tr>


<tr>
<td class="org-left">Guardar quickmark</td>
<td class="org-left">m</td>
</tr>


<tr>
<td class="org-left">Cargar quickmark</td>
<td class="org-left">b</td>
</tr>


<tr>
<td class="org-left">Cargar quickmark en nueva pestaña</td>
<td class="org-left">B</td>
</tr>


<tr>
<td class="org-left">Guardar bookmark</td>
<td class="org-left">M</td>
</tr>


<tr>
<td class="org-left">Cargar bookmark</td>
<td class="org-left">gb</td>
</tr>


<tr>
<td class="org-left">Cargar bookmark en nueva pestaña</td>
<td class="org-left">gB</td>
</tr>


<tr>
<td class="org-left">Guardar la configuración</td>
<td class="org-left">sf</td>
</tr>


<tr>
<td class="org-left">Hacer zoom</td>
<td class="org-left">+</td>
</tr>


<tr>
<td class="org-left">Quitar zoom</td>
<td class="org-left">-</td>
</tr>


<tr>
<td class="org-left">Ver zoom</td>
<td class="org-left">=</td>
</tr>


<tr>
<td class="org-left">Ver código fuente de la página</td>
<td class="org-left">C-S-u</td>
</tr>


<tr>
<td class="org-left">Cerrar todo</td>
<td class="org-left">C-q</td>
</tr>


<tr>
<td class="org-left">Cambiar a pestañas</td>
<td class="org-left">M-[número]</td>
</tr>


<tr>
<td class="org-left">Abrir url actual en mpv</td>
<td class="org-left">U</td>
</tr>


<tr>
<td class="org-left">Abrir video mediante teclado en mpv</td>
<td class="org-left">u</td>
</tr>
</tbody>
</table>
