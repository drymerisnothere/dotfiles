import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')
habitual_packages = open("../../defaults/main.yml").read()\
                                                      .replace('- ', '')\
                                                      .split()[2:]


@pytest.mark.parametrize("name", habitual_packages)
def test_packages(host, name):
    package = host.package(name)
    assert package.is_installed
