Habitual Packages
=========

Install the packages I usually use.

Requirements
------------

None

Role Variables
--------------

* `habitual_packages`: A list containing all the packages.

Dependencies
------------

None

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - { role: habitual-packages }
```

Compatible
----------

With:
  - Debian 9

License
-------

GPLv3

Author Information
------------------

drymer [ AT ] autistici.org
