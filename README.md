# Proveedor de entorno

Este playbook instala lo siguiente:

-   Docker

-   Emacs

-   Fuentes

-   Paquetes de Debian que uso

-   i3blocks-gaps

-   i3-gaps

-   Paquetes de Pip que uso

-   Qutebrowser

-   SLim

-   Syncthing

-   Termite

-   Virtualbox

-   Virtualenvwrapper

Además, también instala mis ficheros de configuración de emacs, i3-wm, qutebrowser y zsh. Básicamente instala todo lo que necesito en un ordenador.

Para poder usarlo, antes hay que instalar algunas dependencias:

    sudo apt install sudo python-pip git
    sudo pip install ansible

Luego copiamos el repositorio:

    git clone https://git.daemons.it/drymer/dotfiles

Instalamos los roles que tiene como dependencias:

    cd dotfiles
    ansible-galaxy install -r requirements.yml -p roles

Y solo queda ejecutar el playbook:

    ansible-playbook install-all.yml -K

Se pueden usar los tags `configuration` o `packages` para instalar solo configuración o paquetes, respectivamente.
